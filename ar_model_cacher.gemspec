$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require File.expand_path('../lib/ar_model_cacher/version', __FILE__)

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ar_model_cacher"
  s.version     = ARModelCacher::VERSION
  s.authors     = ["Stephen Richards"]
  s.email       = ["stephen.richards@tagadab.com"]
  s.homepage    = nil
  s.summary     = "Transparent caching of ActiveRecord Models"
  s.description = "Transparent caching of ActiveRecord Models"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 3.1"
  s.add_dependency "redis"
  s.add_dependency 'memcache-client'
  s.add_dependency 'dalli'
  
  s.add_dependency 'mock_redis'
  s.add_dependency 'memcache_mock'

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "jquery-rails"
  s.add_development_dependency 'yard'
  s.add_development_dependency 'mocha'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'simplecov-rcov'
  s.add_development_dependency 'geminabox'
end
