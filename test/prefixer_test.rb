
require File.expand_path(File.dirname(__FILE__) + '/test_helper')


class Thing
end

module Domain
  module Kingdom 
    module PhylumClass
      class OrderFamilyGenusSpecies
      end
    end
  end
end



class ARModelCacherTest < ActiveSupport::TestCase
  include ARModelCacher

  def test_class_prefix_is_returned_for_single_level_class_name
    assert_equal 'thing', ARModelCacher::Prefixer.prefix(Thing)
  end


  def test_class_prefix_is_returned_for_multi_level_class_name
    assert_equal 'domain__kingdom__phylum_class__order_family_genus_species', ARModelCacher::Prefixer.prefix(Domain::Kingdom::PhylumClass::OrderFamilyGenusSpecies)
  end



  def test_timestamped_prefix_returns_class_prefix_with_timestamp_when_timestamp_already_exists
    ARModelCacher::CacheProxy.expects(:read).with('thing:TS').returns('11112222')
    assert_equal 'thing:11112222', ARModelCacher::Prefixer.timestamped_prefix(Thing)
  end


  def test_timestamped_prefix_creates_a_timestamp_if_one_doesnt_exist
    ARModelCacher::CacheProxy.expects(:read).with('thing:TS').at_least_once.returns(nil)
    Time.stubs(:now).returns(Time.at(333444).utc)
    ARModelCacher::CacheProxy.expects(:write).with('thing:TS', '333444.0')
    assert_equal 'thing:333444.0', ARModelCacher::Prefixer.timestamped_prefix(Thing)
  end



  def test_reset_timestamp_always_resets_the_timestamp
    Time.stubs(:now).returns(Time.at(1234567890).utc)
    ARModelCacher::CacheProxy.expects(:write).with('thing:TS', '1234567890.0')
    ARModelCacher::Prefixer.reset_timestamp(Thing)
  end




end