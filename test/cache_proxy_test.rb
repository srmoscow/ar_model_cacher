require File.expand_path(File.dirname(__FILE__) + '/test_helper')

module ARModelCacher
  class CacheProxyTest < ActiveSupport::TestCase


    def setup
      # Use the config in the dummy app to set up the redis server.
      config = YAML.load_file(File.dirname(__FILE__) + '/dummy/config/ar_model_cacher.yml')['test']
      ARModelCacher::CacheProxy.set_client({'cache_store' => 'mock_redis'})
    end


    def test_that_file_not_there_errors_return_nil
      # Given # that a read on Rails.cache will raise a Errno::ENOENT error
      Rails.cache.stubs(:class).returns(ActiveSupport::Cache::FileStore)

      Rails.cache.stubs(:read).with("myKey").raises(Errno::ENOENT.new('File does not exist'))

      # When I call Rails.cache.read
      result = ARModelCacher::CacheProxy.read('myKey')

      # Then I should get nil
      assert_nil result
    end



    def test_that_exceptions_are_caught
      # Given a rails cache that raises an error
      ARModelCacher::CacheProxy.client
      ARModelCacher::CacheProxy.client.instance_variable_get(:@client).expects(:set).raises(RuntimeError.new('This is an error message'))
      ARModelCacher.logger.expects(:error).with('*************************************************')
      ARModelCacher.logger.expects(:error).with('RuntimeError writing KEY to Redis cache')
      ARModelCacher.logger.expects(:error).with('This is an error message')
      ARModelCacher.logger.expects(:error).with('*************************************************')
      
      ARModelCacher::CacheProxy.write('KEY', 'zzz')
    end


    def test_we_can_write_a_key_and_then_later_retrieve_it 
      ARModelCacher::CacheProxy.write('this is my key', 'And this is my value')
      assert_equal 'And this is my value', ARModelCacher::CacheProxy.read('this is my key')
    end


  end
end


