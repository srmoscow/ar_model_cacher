

require File.expand_path(File.dirname(__FILE__) + '/test_helper')


REDIS_CONFIG = File.dirname(__FILE__) + '/dummy/config/ar_model_cacher.redis.yml'
STD_CONFIG   = File.dirname(__FILE__) + '/dummy/config/ar_model_cacher.yml' unless defined?(STD_CONFIG)


class ARModelCacherTest < ActiveSupport::TestCase
  include ARModelCacher

  fixtures :elements, :uncached_elements, :prices, :uncached_prices



  def setup
    FileUtils.cp REDIS_CONFIG, STD_CONFIG
  end

 

  def test_model_cached_is_true_for_cached_models
    assert Element.model_cached?
  end



  def test_acts_as_cached_is_false_for_uncached_models
    assert_false UncachedElement.model_cached?
  end



  def test_element_can_be_found_by_id_and_then_refound
    e = Element.find 1
    assert_equal 'EL1', e.name
    x = Element.find 1
    assert_equal 'EL1', e.name
  end


  def test_a_collection_of_elements_can_be_stred_and_then_retrieved
    elements = Element.find :all
    retrieved_elements = Element.find :all
    assert_equal elements, retrieved_elements
  end






  def test_find_saves_the_record_if_the_timestamp_exists_but_the_record_is_not_in_cache
    # given an empty record cache, but an existing timestamp
    # when I find an Element by id
    # it should get the timestamp 
    # create a key with the timestamp and class prefix
    # read the cache iwth that key (and get nil)
    # read the record from the database
    # store the recod in cache with the key
    # return the record as a result

    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    ARModelCacher::CacheProxy.expects(:read).with('element:1234567890::33')
    ARModelCacher::CacheProxy.expects(:write).with('element:1234567890::33', 'xxxx')
    Element.expects(:original_find).with(33).returns('xxxx')

    e = Element.find 33
    assert_equal 'xxxx', e
  end


  def test_find_creates_a_new_timestamp_and_saves_the_record_in_cache_if_there_is_no_timestamp_and_no_record
    # given an empty record cache, and no timestamp
    # when I find an Element by id
    # it should get the timestamp (which will return nil)
    # create a new timestamp
    # create a key with the timestamp and class prefix
    # read the cache iwth that key (and get nil)
    # read the record from the database
    # store the recod in cache with the key
    # return the record as a result

    ARModelCacher::CacheProxy.expects(:read).with('element:TS').at_least_once.returns(nil)
    Time.stubs(:now).returns(Time.at(23456789).utc)
    ARModelCacher::CacheProxy.expects(:write).with('element:TS', '23456789.0')
    ARModelCacher::CacheProxy.expects(:read).with('element:23456789.0::33')
    ARModelCacher::CacheProxy.expects(:write).with('element:23456789.0::33', 'xxxx')
    Element.expects(:original_find).with(33).returns('xxxx')

    e = Element.find 33
    assert_equal 'xxxx', e
  end


  def test_find_just_returns_the_record_from_cache_and_doesnt_access_the_database_if_the_record_is_cached
    # Given a record cache containing the record we want
    # When i find an Element by id
    # it should get the timestamp
    # form the key from the timestamp
    # get the record from cache
    # return the record from cache
    # not access the database

    mock_ar_record = mock('ActiveRecord::Base')
    mock_ar_record.stubs(:is_a?).with(Element).returns(true)
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').at_least_once.returns('111222333')
    ARModelCacher::CacheProxy.expects(:read).with('element:111222333::33').returns(mock_ar_record)
    Element.expects(:original_find_by_sql).never

    e = Element.find 33
    assert_equal mock_ar_record, e
  end




  def test_cache_for_model_is_flushed_after_saving_a_new_record
    # given that Time.now will return sequential values in a known range
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when I insert a new element
    new_element = Element.create(:name => 'new element')

    # then the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And find all should now find the extra record
    elements = Element.all
    assert_equal 7, elements.size
  end



  def test_cache_for_model_is_flushed_after_updating_an_existing_record
    # given that Time.now will return sequential values in a known range
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when I update an existing element
    element = elements.first
    element.name = 'updated name'
    element.save!

    # then the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And finding the record should return the updated record
    e2 = Element.find element.id
    assert_equal 'updated name', e2.name
  end


  def test_cache_for_model_is_flushed_after_a_record_is_destroyed
    # given that Time.now will return sequential values in a known range
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when I destroy an existing element
    element = elements.first
    element.destroy

    # then the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And findall all records hsould return one less
    elements = Element.all
    assert_equal 5, elements.size
  end



  def test_cache_for_model_is_flushed_after_a_record_is_deleted
    # given that Time.now will return sequential values in a known range
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when I delete an existing element
    element = elements[2]
    Element.delete(element.id)

    # then the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And findall all records hsould return one less
    elements = Element.all
    assert_equal 5, elements.size
  end




  def test_cache_for_model_is_flushed_when_multiple_records_are_deleted
    # given that Time.now will return sequential values in a known range
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when I delete some elements (including non_existent ones)
    max_id = Element.maximum(:id)
    element_a = elements[2]
    element_b = elements[4]
    result = Element.delete( [ element_a.id, element_b.id, max_id + 1 ] )

    # then the result from the delete should be 2
    assert_equal 2, result

    # and the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And findall all records hsould return one less
    elements = Element.all
    assert_equal 4, elements.size
  end


  def test_cache_for_model_is_not_flushed_when_no_records_are_deleted
    # given that Time.now will return sequential values in a known range
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when call delete passing in ids that don't exist
    max_id = Element.maximum(:id)
    result = Element.delete( [ max_id + 1, max_id + 2, max_id + 3 ] )

    # then the result from the delete should be zero
    assert_equal 0, result

    # and the timestamp should not have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert timestamp == new_timestamp

    # And findall all records hsould return one less
    elements = Element.all
    assert_equal 6, elements.size
  end



  def test_cache_for_model_is_flushed_after_delete_all
    # given that Time.now will return sequential values in a known range
    ARModelCacher::CacheProxy.flush!
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when call delete_all
    result = Element.delete_all

    # then the result from the delete should be 6
    assert_equal 6, result

    # and the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And findall all records should return no records
    elements = Element.all
    assert_equal 0, elements.size

  end



  def test_cache_for_model_is_flushed_after_delete_all_with_mulitple_conditions
    # given that Time.now will return sequential values in a known range
    ARModelCacher::CacheProxy.flush!
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when call delete_all
    result = Element.delete_all(['id = ? or id = ? or id = ?', elements.first.id, elements[1].id, elements.last.id])

    # then the result from the delete should be 6
    assert_equal 3, result

    # and the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And findall all records should return 3 less
    elements = Element.all
    assert_equal 3, elements.size
  end



  def test_cache_for_model_is_flushed_after_delete_all_with_single_condition
    # given that Time.now will return sequential values in a known range
    ARModelCacher::CacheProxy.flush!
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when call delete_all
    result = Element.delete_all('id > 2')

    # then the result from the delete should be 6
    assert_equal 4, result

    # and the timestamp should have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert_false timestamp == new_timestamp
    assert secs.include?(new_timestamp)

    # And findall all records should return 3 less
    elements = Element.all
    assert_equal 2, elements.size
  end



  def test_cache_for_model_not_flushed_when_delete_all_doesnt_delete_any_records
    # given that Time.now will return sequential values in a known range
    ARModelCacher::CacheProxy.flush!
    secs = (Time.now.to_i..Time.now.to_i+200).to_a
    times = secs.map{ |s| Time.at(s) }
    Time.stubs(:now).returns(*times)

    # And the current timestamp for Element is known
    Element.flush_cache
    elements = Element.all
    assert_equal 6, elements.size
    timestamp = ARModelCacher::CacheProxy.read('element:TS').to_i
    assert secs.include?(timestamp)

    # when call delete_all with an non-existing id
    max_id = Element.maximum(:id)
    result = Element.delete_all(['id > ?', max_id])

    # then the result from the delete should be 0
    assert_equal 0, result

    # and the timestamp should NOT have changed
    new_timestamp =  ARModelCacher::CacheProxy.read('element:TS').to_i
    assert timestamp == new_timestamp

    # And findall all records should return the same
    elements = Element.all
    assert_equal 6, elements.size
  end





  def test_update_on_uncached_model
    # no calls should be made to reset the timestamp when I update an uncached model
    element = UncachedElement.first
    ARModelCacher::Prefixer.expects(:reset_timestamp).never
    element.name = 'new name'
    element.save!

  end


  def test_destroy_on_uncached_model
    # no calls should be made to reset the timestamp when I destroy an uncached model
    element = UncachedElement.first
    ARModelCacher::Prefixer.expects(:reset_timestamp).never
    element.destroy
  end


  def test_delete_on_uncached_model_doesnt_attempt_to_flush_cache
    # no calls should be made to reset the timestamp when I delete an uncached model
    elements = UncachedElement.all
    ARModelCacher::Prefixer.expects(:reset_timestamp).never
    result = UncachedElement.delete(elements.first.id, elements.last.id)
    assert_equal 2, result
  end



  def test_delete_all_with_no_params_on_uncached_model_doesnt_attempt_to_flush_cache
    # no calls should be made to reset the timestamp when I delete an uncached model
    elements = UncachedElement.all
    ARModelCacher::Prefixer.expects(:reset_timestamp).never
    result = UncachedElement.delete_all
    assert_equal 6, result
  end


  def test_delete_all_with_params_on_uncached_model_doesnt_attempt_to_flush_cache
    # no calls should be made to reset the timestamp when I delete an uncached model
    elements = UncachedElement.all
    ARModelCacher::Prefixer.expects(:reset_timestamp).never
    result = UncachedElement.delete_all(['id > ?', elements[1].id])
    assert_equal 4, result
  end



  def test_find_does_not_access_memcache_for_uncached_models
    # given a memcache
    # when I find an UncachedElement
    # Rails.cache should not be accessed
    ARModelCacher::CacheProxy.expects(:read).never
    ARModelCacher::CacheProxy.expects(:write).never
    ARModelCacher::CacheProxy.expects(:fetch).never

    UncachedElement.find 5
  end






  def test_flush_cache_for_a_model_clears_all_the_cache_for_that_model_only
    # Given a set timestamp for Element and Price
    ARModelCacher::CacheProxy.set_client({'cache_store' => 'mock_redis'})
    ARModelCacher::CacheProxy.write('element:TS', '111222333')
    ARModelCacher::CacheProxy.write('price:TS', '111222333')
    
    et1 = Element.cache_timestamp
    pt1 = Price.cache_timestamp
    assert_equal '111222333', et1
    assert_equal '111222333', pt1

    # When I flush cache, the timestamp for Element should be set
    Element.flush_cache
    
    
    # The timestamp for element should be reset and that fro Price untouched
    et2 =  Element.cache_timestamp
    pt2 =  Price.cache_timestamp

    assert_equal pt1, pt2
    assert_not_equal et1, et2
  end


  def test_flush_cache_without_parameters_does_not_flush_cache_for_associations
    Element.expects(:flush_cache).at_most_once
    Price.expects(:flush_cache).never

    Element.flush_cache
  end


  def test_flush_cach_with_true_param_flushes_cache_on_associated_cached_models
    Element.expects(:flush_cache).at_most_once
    Price.expects(:flush_cache).at_most_once

    Element.flush_cache(true)
  end



  def test_that_scopes_without_parameters_can_be_stored
    # Given an empty cache
    # when I call Element.inactive
    # it should get the timestamp
    # form an MD5 hash from the sql query and use that as a key
    # store the record array if the key doesn't exist
    # retutn the record array
    #
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    sql = %Q{SELECT "elements".* FROM "elements"  WHERE "elements"."active" = 'f'}
    digest = Digest::MD5.hexdigest(sql)
    key = "element:1234567890::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns(nil)
    Element.expects(:original_find_by_sql).returns(['aaaa', 'bbbb', 'cccc'])
    ARModelCacher::CacheProxy.expects(:write).with(key, ['aaaa', 'bbbb', 'cccc'])
   
    arr = Element.inactive
    assert_equal ['aaaa', 'bbbb', 'cccc'], arr
  end



  def test_that_scopes_without_parameters_can_be_retrieved
    # Given a cach with an entry for the inactive scope
    # When I call Element.inactive
    # it should get the timestamp
    # form the key from the hashed sql and the timestamp
    # and return the result from memcached
    # and the database should not be accessed
    #


    ar1 = mock('ActiveRecord::Base')
    ar2 = mock('ActiveRecord::Base')
    ar3 = mock('ActiveRecord::Base')
    ar1.stubs(:is_a?).with(Element).returns(true)
    ar2.stubs(:is_a?).with(Element).returns(true)
    ar3.stubs(:is_a?).with(Element).returns(true)
    
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    sql = %Q{SELECT "elements".* FROM "elements"  WHERE "elements"."active" = 'f'}
    digest = Digest::MD5.hexdigest(sql)
    key = "element:1234567890::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns( [ar1, ar2, ar3]  )
    Element.expects(:original_find_by_sql).never

    arr = Element.inactive
    assert_equal  [ar1, ar2, ar3] , arr
  end







  def test_that_scopes_with_parameters_can_be_stored_and_retrieved
    # Given an empty cache
    # when I call Element.between(80, 90)
    # It should form a key from the tiemstamp and the hash of the sql
    # attempt to get the record from memcache
    # get the record set from the db
    # write the record set to memcache
    # return the record set

    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    digest = Digest::MD5.hexdigest %Q{SELECT "elements".* FROM "elements"  WHERE (id > 80 and id < 90)}
    key = "element:1234567890::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns(nil)
    Element.expects(:original_find_by_sql).returns( %w{ ggg hhh iii } )
    ARModelCacher::CacheProxy.expects(:write).with(key, %w{ ggg hhh iii })

    record_set = Element.between(80,90)
    assert_equal %w{ ggg hhh iii }, record_set
  end 



  def test_that_scopes_with_parameters_already_in_cache_can_be_retrieved
    # Given an empty cache
    # when I call Element.between(80, 90)
    # It should form a key from the tiemstamp and the hash of the sql
    # Get the record from memcache
    # Not access the record set from the db
    # return the record set


    ar1 = mock('ActiveRecord::Base')
    ar2 = mock('ActiveRecord::Base')
    ar3 = mock('ActiveRecord::Base')
    ar1.stubs(:is_a?).with(Element).returns(true)
    ar2.stubs(:is_a?).with(Element).returns(true)
    ar3.stubs(:is_a?).with(Element).returns(true)

    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    digest = Digest::MD5.hexdigest %Q{SELECT "elements".* FROM "elements"  WHERE (id > 80 and id < 90)}
    key = "element:1234567890::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns( [ar1, ar2, ar3] )
    ARModelCacher::CacheProxy.expects(:write).with(key, instance_of(Array)).never
    Element.expects(:original_find_by_sql).never

    record_set = Element.between(80,90)
    assert_equal [ar1, ar2, ar3], record_set

  end   



 
  def test_that_find_by_all_attribute_name_operations_are_stored_in_cache_and_can_be_retrieved_from_cache
    # Given an empty cache
    # When I call Element.find_all_by_name
    # It should form the key from the timestamp and hash of sql query
    # it should check if the record set is stored in cache
    # if not it should retreive the record set from the DB and store it in the cache
    # it should return the record set
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    digest = Digest::MD5.hexdigest %q{SELECT "elements".* FROM "elements"  WHERE "elements"."name" = 'EL59'}
    key = "element:1234567890::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns(nil)
    ARModelCacher::CacheProxy.expects(:write).with(key, Array.new)
    Element.expects(:original_find_by_sql).returns(Array.new)

    result = Element.find_all_by_name('EL59')
    assert_equal Array.new, result
  end


  def test_that_find_by_attribute_name_returns_the_record_set_stored_in_cache
    # Given an cahe with the requested record set
    # When I call Element.find_all_by_name
    # It should form the key from the timestamp and hash of sql query
    # it should check if the record set is stored in cache
    # it should return the record set from cache
    # it should not access the database
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('1234567890')
    digest = Digest::MD5.hexdigest %q{SELECT "elements".* FROM "elements"  WHERE "elements"."name" = 'EL59'}
    key = "element:1234567890::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns(Array.new)
    Element.expects(:original_find_by_sql).never

    result = Element.find_all_by_name('EL59')
    assert_equal Array.new, result
  end






  def test_cached_associations_are_stored_in_cache
    # given an cache with one element record
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('111222333').at_least_once
    ARModelCacher::CacheProxy.expects(:read).with('element:111222333::4').returns(Element.original_find_by_sql('select * from elements where id = 4').first)
    element = Element.find 4

    # when I call element.prices
    # then should read the cache to see if the associated records are there
    # then store the result if not
    # and return the result
    sql = %q{SELECT "prices".* FROM "prices"  WHERE "prices"."element_id" = 4}
    digest = Digest::MD5.hexdigest sql
    ARModelCacher::CacheProxy.expects(:read).with('price:TS').returns('22233344')
    key = "price:22233344::#{digest}"
    ARModelCacher::CacheProxy.expects(:read).with(key).returns(Price.original_find_by_sql(sql))
    arel = element.prices
    prices  = arel.all

    # And the result should be an array of Price records belonging to element 4
    assert_instance_of Array, prices
    assert_instance_of Price, prices.first
    assert_equal [4], prices.map(&:element_id).uniq

  end




  def test_uncached_models_are_not_cached
    # given an uncached model
    # when I find the model 
    # the cache should never be read or written to
    ARModelCacher::CacheProxy.expects(:read).never
    UncachedElement.find(3).prices
  end



  def test_uncached_associations_on_cached_models_are_not_cached
    # given a cached model 
    # with an association to an uncached model
    ARModelCacher::CacheProxy.expects(:read).with('element:TS').returns('111222333').at_least_once
    ARModelCacher::CacheProxy.expects(:read).with('element:111222333::4').returns(Element.original_find_by_sql('select * from elements where id = 4').first)
    
    e = Element.find 4
    # when I read the association
    # there should be no calles to the cache
    ARModelCacher::CacheProxy.expects(:read).never

    ups = e.uncached_prices
    ups.all
    assert_equal 2, ups.size
  end


  def test_that_cache_is_not_flushed_if_the_record_is_not_saved
    # given an element
    e = Element.find 4

    # when I save it without changing any attributes
    Element.expects(:flush_cache).never
    e = Element.find 4
  end


  def test_that_the_cache_is_flushed_when_a_model_is_updated
    # given an element
    e = Element.find 4

    # when I update it, it should call flush for the model
    Element.expects(:flush_cache)
    e.name = 'xxx'
    e.save
  end


  def test_that_after_save_callbacks_are_called_as_well_as_the_flush_cache
    # Given an element
    e = Element.find 4

    # When I update it, it shuld call flush for the model AND the after save callback defined in the model
    Element.expects(:flush_cache)
    e.expects(:element_defined_after_save_callback)

    e.name = 'xxx'
    e.save
  end




  def test_associated_classes_returns_the_correct_list
    assert_equal ['Price', 'UncachedPrice'], Element.associated_classes.map(&:to_s).sort
  end



  def test_associated_cached_classes_returns_the_correct_list
    assert_equal ['Price'], Element.associated_cached_classes.map(&:to_s)
  end


  
end


