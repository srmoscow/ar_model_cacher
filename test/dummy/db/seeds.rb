

Element.delete_all

(1..100).each do |i|
  e = Element.new(:name => "EL#{i}", :description => "Element #{i}", :active => !(i % 5 == 0))
  e.id = i
  e.save!
end


UncachedElement.delete_all
(1..100).each do |i|
  e = UncachedElement.new(:name => "EL#{i}", :description => "Element #{i}", :active => !(i % 5 == 0))
  e.id = i
  e.save!
end


Price.delete_all
Element.all.each do |element|
  Price.create!(:element_id => element.id, :price => element.id * 5, :startdate => 1.week.ago)
  Price.create!(:element_id => element.id, :price => element.id * 5, :startdate => 1.month.from_now) if element.id % 4 == 0
end
