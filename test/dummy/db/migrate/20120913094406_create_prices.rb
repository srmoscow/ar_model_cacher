class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.integer :element_id
      t.integer :price
      t.date :startdate

      t.timestamps
    end
  end
end
