class UncachedElement < ActiveRecord::Base
  attr_accessible :active, :description, :name

  has_many :prices
end
