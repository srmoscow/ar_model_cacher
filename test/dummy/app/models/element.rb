class Element < ActiveRecord::Base

  cache_model
  
  attr_accessible :active, :description, :name

  has_many :prices

  has_many :future_prices, :class_name => 'Price', :conditions => (['startdate > ?', Date.today]) 

  has_many :uncached_prices

  scope :inactive, where(:active => false)

  scope :newish, lambda { |x| { :conditions => ['id > ?', x] } }

  scope :between, lambda { |x, y| { :conditions => ['id > ? and id < ?', x, y] } }

  after_save :element_defined_after_save_callback

  def element_defined_after_save_callback
    true
  end

end
