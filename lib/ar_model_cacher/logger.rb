require 'socket'

module ARModelCacher


  class Logger 


    def initialize(filename, onoroff)

      @active   = onoroff
      @logger   = ::Logger.new(filename) if @active == true
      @hostname = Socket.gethostname
    end


    def debug(message)
      @logger.debug("#{prefix} DEBUG #{message}") if @active == true
    end

    def error(message)
      @logger.debug("#{prefix} ERROR #{message}") if @active == true
    end


    private 
    def prefix
      "#{Time.now.strftime('%Y-%m-%d %H:%M:%S.%6N')} #{@hostname}"
    end


  end
end
