require 'ar_model_cacher'
require 'rails'

module ARModelCacher
  class Railtie < Rails::Railtie
    rake_tasks do
      load "tasks/ar_model_cacher.rake"
    end
  end
end
