module ARModelCacher


  # This class provides a method to convert classes and names of classes to a standardised normalised string
  # that can be used in memcache keys
  class Prefixer


    # returns the class prefix, e.g. Element => element, Pricing::Price => pricing__price
    def self.prefix(klass)
      klass.to_s.gsub('::', '__').underscore
    end



    # returns the class_prefix and timestamp, et.g pricing_price:1234567890
    def self.timestamped_prefix(klass)
      class_prefix = prefix(klass)
      "#{class_prefix}:#{timestamp(class_prefix)}"
    end



    def self.timestamp(class_prefix)
      timestamp_value = ARModelCacher::CacheProxy.read("#{class_prefix}:TS")
      ARModelCacher.logger.debug "TimestampValue #{timestamp_value.inspect} returned for key #{class_prefix}:TS"
      if timestamp_value.nil?
        timestamp_value = write_timestamp(class_prefix)
      end
      timestamp_value
    end



    def self.write_timestamp(class_prefix)
      timestamp_value = new_timestamp_value(class_prefix)
      ARModelCacher::CacheProxy.write("#{class_prefix}:TS", timestamp_value)
      ARModelCacher.logger.debug "TimestampValue #{timestamp_value} written for key #{class_prefix}:TS"
      timestamp_value
    end


    # this method guarantees the prefix is changed and can never go backwards (where it might clash with an old value)
    def self.new_timestamp_value(class_prefix)
      timestamp_value = Time.now.utc.to_f.to_s
      current_timestamp = ARModelCacher::CacheProxy.read("#{class_prefix}:TS")
      if current_timestamp && current_timestamp.respond_to?(:to_f) && (current_timestamp == timestamp_value || current_timestamp.to_f > timestamp_value.to_f)
        timestamp_value = (current_timestamp.to_f + 1).to_s
      end
      timestamp_value
    end


    def self.reset_timestamp(klass)
      class_prefix = prefix(klass)
      write_timestamp(class_prefix)
    end



  end

end