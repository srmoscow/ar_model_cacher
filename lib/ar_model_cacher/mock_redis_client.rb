module ARModelCacher
  class MockRedisClient < RedisClient

    def initialize(config)
      require 'mock_redis'
      @client = MockRedis.new
      ARModelCacher.logger.debug "Using RedisMock client"
    end

  end
end