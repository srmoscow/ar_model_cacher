module ARModelCacher

  # This class acts as a proxy for Rails.cache, and gracefully handles Errno::ENOENT errors when the cache is a filestore
  class CacheProxy

    @@client = nil


    # This should be set from ARModelCacher.initialize!
    def self.set_client(config)
      case config['cache_store']
      when 'dalli'
        require File.dirname(__FILE__) + '/dalli_client'
        @@client = ARModelCacher::DalliClient.new(config)

      when 'memcached'
        require File.dirname(__FILE__) + '/memcache_client'
        @@client = ARModelCacher::MemcacheClient.new(config)

      when 'mock_memcached'
        require File.dirname(__FILE__) + '/memcache_client'
        require File.dirname(__FILE__) + '/mock_memcache_client'
        @@client = ARModelCacher::MockMemcacheClient.new(config)

      when 'redis'
        require File.dirname(__FILE__) + '/redis_client'
        @@client = ARModelCacher::RedisClient.new(config)

      when 'mock_redis'
        require File.dirname(__FILE__) + '/redis_client'
        require File.dirname(__FILE__) + '/mock_redis_client'
        @@client = ARModelCacher::MockRedisClient.new(config)

      else
        raise "Invalid cache store specified in ar_model_cacher.yml"
      end
    end




    def self.client
      raise 'No client initialized' if @@client.nil?
      @@client
    end


    # only used from tests  and after migrations - flushes all keys in the database (but not other databases)
    def self.flush!

      client.flush!
    end


    

    def self.read(key)
      begin
        result = client.read(key)
      rescue => err
        ARModelCacher.logger.error "*************************************************"
        ARModelCacher.logger.error "#{err.class} reading #{key} from #{client.type} cache"
        ARModelCacher.logger.error "#{err.message}"
        ARModelCacher.logger.error "*************************************************"
        result = nil
      end
      result
    end


    def self.write(key, value)
      begin
        result = client.write(key, value)
      rescue => err
        ARModelCacher.logger.error "*************************************************"
        ARModelCacher.logger.error "#{err.class} writing #{key} to #{client.type} cache"
        ARModelCacher.logger.error "#{err.message}"
        ARModelCacher.logger.error "*************************************************"
        result = nil
      end
      result
    end
  

  end

end