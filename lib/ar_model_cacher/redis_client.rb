module ARModelCacher


  class RedisClient


    def initialize(config)
      require 'redis'
      begin
        @client = Redis.new(:host => config['host'], :port => config['port'].to_i, :db => config['db'].to_i )
        # check we can get keys to test whether the server is actually up
        @client.keys 'xxxx'
      rescue Redis::CannotConnectError => err
        puts "REDIS CONNECTION ERROR: Have you got a redis server running on #{config['host']}:#{config['port']}?"
        raise err
      end
      ARModelCacher.logger.debug "Connected to Redis server on #{config['host']}:#{config['port']}/db#{config['db']}"
    end



    # only used from tests  and after migrations - flushes all keys in the database (but not other databases)
    def flush!
      @client.flushdb
    end


    

    # Read the cache, catching any errors, logging them and returning nil
    def read(key)
      result = @client.get(key)
      return_val = result.nil? ? nil : YAML::load(result)
    end

      
    def type
      'Redis'
    end




    def write(key, value)
      serialized_value = value.to_yaml
      @client.set(key, serialized_value)
      serialized_value
    end



  end


end