module ARModelCacher

  class MockMemcacheClient < MemcacheClient


    def initialize(config)
      require "memcache_mock"
      @client = MemcacheMock.new
      ARModelCacher.logger.debug "Using MemcacheMock client"
    end

  end


end