module ARModelCacher


  class MemcacheClient


    def initialize(config)
      require 'memcache'
      begin
        if config['host'].is_a?(Array)
          host_and_port = config['host'].map{|host| "#{host}:#{config['port']}"}
        else
          host_and_port = "#{config['host']}:#{config['port']}"
        end
        @client = MemCache.new(host_and_port, :namespace => 'ARMC')
        
        # check we can write a key and retrieve it to test whether the server is actually up
        if host_and_port.is_a?(Array)
          host_and_port.each do |connect_to|
            temp_client = MemCache.new(connect_to, :namespace => 'ARMC')
            temp_client.set 'my_test_key', 'my_test_data'
          end
        else
          @client.set 'my_test_key', 'my_test_data'
        end
        raise "Unable to retrieve test key from MemCache server" unless @client.get('my_test_key') == 'my_test_data'
      rescue MemCache::MemCacheError => err
        puts "MEMCACHE CONNECTION ERROR: Have you got a MemCache server running on #{config['host']}:#{config['port']}?"
        raise err
      end
      ARModelCacher.logger.debug "Connected to Memcache server on #{config['host']}:#{config['port']}"
    end



    # only used from tests  and after migrations - flushes all keys in the memcache store 
    def flush!
      @client.flush_all
    end


    

    # Read the cache, catching any errors, logging them and returning nil
    def read(key)
      result = @client.get(key)
      result
    end

      
    def type
      'MemCache'
    end




    def write(key, value)
      @client.set(key, value)
      value
    end



  end


end