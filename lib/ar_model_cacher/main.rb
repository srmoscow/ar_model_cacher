
require 'active_support/concern'
require File.dirname(__FILE__) + '/logger'





module ARModelCacher
 extend ActiveSupport::Concern

  included do
  end

  @@initialized = false

  def self.initialize!
    return if @@initialized == true
    raise "No config/ar_model_cacher.yml file found" unless File.exist?("#{Rails.root}/config/ar_model_cacher.yml")
    @@config = YAML.load(ERB.new(File.read("#{Rails.root}/config/ar_model_cacher.yml")).result)
    raise "No section for #{Rails.env} Rails environment found in /config/ar_model_cacher.yml" if @@config[Rails.env].nil?

    @@model_cacher_log ||= ::ARModelCacher::Logger.new("#{Rails.root}/log/ar_model_cacher.log", ARModelCacher.logging_enabled?)

    if ARModelCacher.caching_enabled?
      ARModelCacher::CacheProxy.set_client(@@config[Rails.env])
      ARModelCacher.logger.debug "ARModelCacher version #{ARModelCacher::VERSION} started."
    else
      ARModelCacher.logger.debug "ARModelCacher version #{ARModelCacher::VERSION} initialized with caching disabled."
    end
    
    @@initialized = true
  end


  def self.logger
    raise "Boom!" if @@model_cacher_log.nil?
    @@model_cacher_log
  end

 
  def self.logging_enabled?
    raise "No log directive found in #{Rails.env} section of config/ar_model_cacher.yml" if @@config[Rails.env]['log'].nil?
    return @@config[Rails.env]['log'] 
  end


  def self.caching_enabled?
    raise "No log directive found in #{Rails.env} section of config/ar_model_cacher.yml" if @@config[Rails.env]['cache'].nil?
    return @@config[Rails.env]['cache'] 
  end


  def self.initialized?
    @@initialized
  end






  module ClassMethods
    
    # Enable caching on this model.
    #
    # Parameters:
    #
    # * options: a hash containing the following optional values:
    #   - :scopes: a symbol or array of symbols specifying the scoped finds that are to be cached
    #
    #   - :attributes: a symbol or array of symbols specifying the find_by_attribute_name finds that are to be cached.
    #
    # * Example
    #
    #     ar_model_cacher :scopes => :inactive, :attributes => [:name, :name_and_active]
    #
    # The above example on Element will cause the result sets of the following methods to be cached:
    #
    #     Element.inactive
    #     Element.find_by_name('xxxx')
    #     Element.find_all_by_name('xxxx')
    #     Element.find_by_name_and_active('xxxx', false)
    #     Element.find_all_by_name_and_active('xxxx', false)
    #
    def cache_model(options = {})
      ARModelCacher.initialize! unless ARModelCacher.initialized?
      unless ARModelCacher.caching_enabled?
        ARModelCacher.logger.debug "Model Caching for #{self} not enabled as caching is disabled in config"
        return
      end
      ARModelCacher.logger.debug "Model Caching enabled for #{self}"
     
      # maintain a list of models that are cached
      @@is_cached ||= Hash.new
      @@is_cached[class_prefix] = true
      ARModelCacher.logger.debug "Models now cached are: #{@@is_cached.keys.sort.join(', ')}"


      # define an after save callback to flush the cache
      class_eval <<-"end_eval"
          after_save :flush_cache_afer_save
          before_destroy :flush_cache_before_destroy
          def flush_cache_afer_save
            #{self}.flush_cache
          end

          def flush_cache_before_destroy
            #{self}.flush_cache
          end


      end_eval


    end



    def enable_caching
      @@is_cached ||= Hash.new
      @@is_cached[class_prefix] = true
      flush_cache
    end



    def disable_caching
      @@is_cached ||= Hash.new
      @@is_cached[class_prefix] = false
    end




    # Returns true if the model uses ar_model_caching, otherwise false
    def model_cached?
      @@is_cached ||= Hash.new
      @@is_cached[class_prefix] || false
    end



    # deletes all stored keys for the model, optionally deleteing the cache for all associated models as well.
    def flush_cache(flush_associations = false)
      if model_cached?
        ARModelCacher.logger.debug "flush cache called for #{self}"
        Prefixer.reset_timestamp(self)                                       
      end
      if flush_associations
        self.associated_cached_classes.each { |k| k.flush_cache }
      end
    end    



    # Returns an array of associated classes to this model
    def associated_classes
      self.reflect_on_all_associations.map(&:klass).uniq
    end


    def associated_cached_classes
      self.associated_classes.select{ |k| k.model_cached? }
    end
    


    def cache_timestamp
      class_prefix = ARModelCacher::Prefixer.prefix(self)
      Prefixer.timestamp(class_prefix)
    end



   

    private


    # generates a prefix based on the class name suitable for keys
    def class_prefix
      Prefixer.prefix(self)
    end



    def timestamped_class_prefix
      Prefixer.timestamped_prefix(self)
    end


    # retursn the full key (the key for storing the record, and abbreviated key (for storing in the cache_key index) for the given arguments.
    def keyify(*args)
      "#{timestamped_class_prefix}::#{args.join(':')}"
    end

   
  end
 
end





class ActiveRecord::Base

  class << self

    alias_method  :original_find, :find
    alias_method  :original_find_by_sql, :find_by_sql
    alias_method  :original_delete, :delete
    alias_method  :original_delete_all, :delete_all





    # we cache calls to find(n) here because the to_sql method in find_by_sql blows up when used after at least one call to find_by_sql for one of these types of find
    def find(*args)
      if model_cached?  && args.size == 1 && args.first.is_a?(Fixnum)
        ARModelCacher.logger.debug "find arguments: #{args.inspect}"
        key = keyify(*args)
        result = fetch_from_cache(key) do
          self.original_find(*args)
        end
      else
        original_find(*args)
      end
    end


    # The only thing that has a binds parameter is find(n).  They are dealt with in find above, so we handle everything else
    # note that we can't log the sql if binds are not empty - it blows up.
    def find_by_sql(arel, binds=[])
      if model_cached? && binds.empty?
        sql = arel.to_sql
        ARModelCacher.logger.debug "find_by_sql arel.to_sql: #{arel.to_sql}"
        digest = Digest::MD5.hexdigest(sql)
        key = "#{timestamped_class_prefix}::#{digest}"
        ARModelCacher.logger.debug "#{digest} generated from #{sql}"
        result = fetch_from_cache(key) do
          self.original_find_by_sql(arel, binds)
        end
        return result
     else
        self.original_find_by_sql(arel, binds)
      end
    end



    # we want to make sure we flush cahe for the model if a record is deleted
    def delete(*args)
      if model_cached? 
        result = self.original_delete(args)
        unless result == 0
          self.flush_cache
        end
      else
        result = self.original_delete(args)
      end
      result
    end


    # we want to flush the cache after a delete all
    def delete_all(conditions = nil)
      if model_cached? 
        result = self.original_delete_all(conditions)
        unless result == 0
          self.flush_cache
        end
      else
        result = self.original_delete_all(conditions)
      end
      result
    end



    # new fetch from cache which will log if logging switched on.
    def fetch_from_cache(key, *args, &block)
      unless key.is_a?(String)
        ARModelCacher.logger.error "Unexpected key passed to fetch_from_cache(): #{key}"
        ARModelCacher.logger.error caller.join("\n")
      end
      result = ARModelCacher::CacheProxy.read(key)

      if result.nil? || result_invalid?(result, key)
        result = block.call(*args)
        ARModelCacher::CacheProxy.write(key, result)
        ARModelCacher.logger.debug "#{key} written to cache"
      else
        ARModelCacher.logger.debug "#{key} read from cache: returned instance of #{result.class}"
      end
      result
    end

     
    # We have a persistant problem whereby the result returned from Rails.cache is not an ActiveRecord object or a collection thereof.
    # This code logs the details and also tries to re-read it.
    def result_invalid?(result, key, attempt_retry = true)
      ARModelCacher.logger.debug "**** CLASS IDENTIFIER #{self}"
      invalid = false
      if result.class == Array
        ARModelCacher.logger.debug "Array containing #{result.size} instances comprising #{result.map(&:class).map(&:to_s).uniq} returned for key #{key}"
        result.each do |r|
          # unless r.is_a?(ActiveRecord::Base)
          unless r.is_a?(self)
            ARModelCacher.logger.error " **********  XXXX Unexpected result retrieved from ar_model_cacher for key #{key}:: #{r.inspect} - expected #{self}  got #{r.class}"
            ARModelCacher.logger.error caller.join("\n")
            invalid = true
          end
        end
      else
        ARModelCacher.logger.debug "Object of class #{result.class} returned for key #{key}"
        unless result.is_a?(self)
          ARModelCacher.logger.error " ************  XXXX Unexpected result retrieved from ar_model_cacher for key #{key}:: #{result.inspect} - expected #{self} got #{result.class}"
          ARModelCacher.logger.error caller.join("\n")

          if attempt_retry == true
            ARModelCacher.logger.error " ************  Trying again with key #{key}:: #{result.inspect}"
            result = ARModelCacher::CacheProxy.read(key)
            if result_invalid?(result, key, false)
              ARModelCacher.logger.error " ************  Unexpected result retrieved second time round from ar_model_cacher for key #{key}:: #{result.inspect}"
            else
              ARModelCacher.logger.error " ************  Got an OK result second time round for key #{key}"
           end
         end
         invalid = true
        end
      end
      invalid
    end



  end

end



 
ActiveRecord::Base.send :include, ARModelCacher









