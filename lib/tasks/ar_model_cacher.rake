namespace :ar_model_cacher do
  desc 'Flushes AR Model Cacher for all models'
  task :flush => :environment do
    require 'socket'
    ARModelCacher::CacheProxy.flush!
    puts "ARModelCacher cache flushed for all models on #{Socket::gethostname}"
  end
end
